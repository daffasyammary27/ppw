from django.shortcuts import render, redirect, get_object_or_404
from .models import Kegiatan, Peserta
from .forms import KegiatanForm, PesertaForm

# Create your views here.

def lihatKegiatan(request):
    kegiatan = Kegiatan.objects.all()
    peserta = Peserta.objects.all()
    return render(request, 'lihatKegiatan.html', {'kegiatan':kegiatan, 'peserta':peserta})


def tambahKegiatan(request):
    if request.method == "POST":
        form = KegiatanForm(request.POST)

        if form.is_valid():
            form.save()
            return redirect('story6:lihatKegiatan')

    else:
        form = KegiatanForm()

    return render(request, 'tambahKegiatan.html', {'form':form})

def tambahPeserta(request, ids):
    if request.method == "POST":
        form = PesertaForm(request.POST)

        if form.is_valid():
            peserta = Peserta(ikutKegiatan=Kegiatan.objects.get(id=ids), Nama_Peserta = form.data['Nama_Peserta'])
            peserta.save()
            return redirect('../../')
    
    else:
        form = PesertaForm()

    return render(request, 'tambahPeserta.html', {'form':form})

def hapusPeserta(request, ids):
    hapus_peserta = get_object_or_404(Peserta, id = ids)

    if request.method == "POST":
        hapus_peserta.delete()
        return redirect('../')

    return render(request, 'hapusPeserta.html', {'hapus_peserta':hapus_peserta})