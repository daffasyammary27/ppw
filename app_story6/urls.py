from django.urls import path
from . import views

app_name = 'story6'

urlpatterns = [
    path('', views.tambahKegiatan, name='tambahKegiatan'),
    path('lihat/', views.lihatKegiatan, name='lihatKegiatan'),
    path('lihat/register/<int:ids>/', views.tambahPeserta, name='tambahPeserta'),
    path('lihat/<int:ids>/', views.hapusPeserta, name='hapusPeserta')
]
