from django.test import TestCase, Client
from django.urls import resolve
from .views import lihatKegiatan, tambahKegiatan, tambahPeserta, hapusPeserta
from .models import Kegiatan, Peserta

# Create your tests here.
class TestKegiatan(TestCase):
	def test_event_url_is_exist(self):
		response = Client().get('/kegiatan/lihat/')
		self.assertEqual(response.status_code, 200)

	def test_event_index_func(self):
		found = resolve('/kegiatan/lihat/')
		self.assertEqual(found.func, lihatKegiatan)

	def test_event_using_template(self):
		response = Client().get('/kegiatan/lihat/')
		self.assertTemplateUsed(response, 'lihatKegiatan.html')

class TestTambahKegiatan(TestCase):
	def test_add_event_url_is_exist(self):
		response = Client().get('/kegiatan/')
		self.assertEqual(response.status_code, 200)

	def test_add_event_index_func(self):
		found = resolve('/kegiatan/')
		self.assertEqual(found.func, tambahKegiatan)

	def test_add_event_using_template(self):
		response = Client().get('/kegiatan/')
		self.assertTemplateUsed(response, 'tambahKegiatan.html')

	def test_event_model_create_new_object(self):
		acara = Kegiatan(Nama_Kegiatan="abc")
		acara.save()
		self.assertEqual(Kegiatan.objects.all().count(), 1)

	def test_event_url_post_is_exist(self):
		response = Client().post('/kegiatan/', data={'Nama_Kegiatan':'menyanyi'})
		self.assertEqual(response.status_code, 302)

class TestRegist(TestCase):
	def setUp(self):
		acara = Kegiatan(Nama_Kegiatan="abc")
		acara.save()

	def test_regist_url_post_is_exist(self):
		response = Client().post('/kegiatan/lihat/register/1/', data={'Nama_Peserta':'irham'})
		self.assertEqual(response.status_code, 302)
	
	def test_regist_url_is_exist(self):
		response = Client().get('/kegiatan/lihat/register/1/')
		self.assertEqual(response.status_code, 200)

class TestHapusNama(TestCase):
	def setUp(self):
		acara = Kegiatan(Nama_Kegiatan="abc")
		acara.save()
		nama = Peserta(Nama_Peserta="cba")
		nama.save()

	def test_hapus_url_post_is_exist(self):
		response = Client().post('/kegiatan/lihat/1/')
		self.assertEqual(response.status_code, 302)
	
	def test_hapus_url_is_exist(self):
		response = Client().get('/kegiatan/lihat/1/')
		self.assertEqual(response.status_code, 200)
