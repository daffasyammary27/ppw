from django.shortcuts import render, redirect

# Create your views here.
from django.http import HttpResponse
import datetime
from .models import Schedule
from .forms import ScheduleForm



def home(request):
    return render(request,'index.html')

def about(request):
    return render(request, 'profile.html')
