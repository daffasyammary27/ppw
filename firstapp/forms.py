from django import forms
from .models import Schedule

class ScheduleForm(forms.ModelForm):
    class Meta:
        model = Schedule
        fields = ['mataKuliah', 'dosenPengajar', 'jumlahSKS', 'semesterTahun', 'ruang', 'deskripsi']